import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('@/components/Home.vue')
  },
  {
    path: '/questions',
    name: 'Questions',
    component: () => import('@/components/Questions/Questions.vue')
  },
  {
    path: '/results',
    name: 'Results',
    component: () => import('@/components/Results/Results.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: routes,
});

export default router;