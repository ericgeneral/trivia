function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

function randomizeMultipleChoiceAnswers(question) {
  const random = getRandomInt(4);
  let answers = [];
  answers[random % 4] = { value: question.correct_answer, correct: true };
  for (let i = 0; i < question.incorrect_answers.length; i++) {
    answers[(random + i + 1) % 4] = { value: question.incorrect_answers[i], correct: false };
  }
  return answers;
}

function organizeTrueFalseAnswers(question) {
  let answers = [];
  if (question.correct_answer === 'True') {
    answers[0] = { value: 'True', correct: true };
    answers[1] = { value: 'False', correct: false };
  } else {
    answers[0] = { value: 'True', correct: false };
    answers[1] = { value: 'False', correct: true };
  }
  return answers;
}

class AnswerRandomizer {
  randomizeAnswerOrder(questions) {
    questions.forEach(question => {
      if (question.type === 'multiple') {
        question.randomizedAnswers = randomizeMultipleChoiceAnswers(question);
      } else {
        question.randomizedAnswers = organizeTrueFalseAnswers(question);
      }
    });
  }
}

export default AnswerRandomizer;