import Vue from 'vue';
import Vuex from 'vuex';
import { OpentdbAPI } from '@/components/Opentdb/OpentdbAPI';
import AnswerRandomizer from '@/store/answerRandomizer.js';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    difficulty: '',
    category: 0,
    categories: [],
    numberOfQuestions: 0,
    questions: [],
    correctCount: 0,
    currentQuestion: 0,
    error: '',
    gameOver: false,
  },
  mutations: {
    setDifficulty: (state, payload) => {
      state.difficulty = payload;
    },
    setCategory: (state, payload) => {
      state.category = payload;
    },
    setCategories: (state, payload) => {
      state.categories = payload;
    },
    setNumberOfQuestions: (state, payload) => {
      state.numberOfQuestions = payload;
    },
    setQuestions: (state, payload) => {
      state.questions = payload;
    },
    setError: (state, payload) => {
      state.error = payload;
    },
    incrementCorrectCount: (state) => {
      state.correctCount += 1;
    },
    incrementCurrentQuestion: (state) => {
      if (state.currentQuestion === state.numberOfQuestions - 1) {
        state.gameOver = true;
      } else {
        state.currentQuestion += 1;
      }
    },
    addUserAnswer: (state, payload) => {
      state.questions[state.currentQuestion].user_answer = payload;
    },
    resetState: (state) => {
      state.difficulty = '';
      state.category = 0;
      state.categories = [];
      state.numberOfQuestions = 0;
      state.questions = [];
      state.userAnswers = [];
      state.correctCount = 0;
      state.currentQuestion = 0;
      state.error = '';
      state.gameOver = false;
    },
    playAgain: (state) => {
      state.categories = [];
      state.questions = [];
      state.userAnswers = [];
      state.correctCount = 0;
      state.currentQuestion = 0;
      state.error = '';
      state.gameOver = false;
    }
  },
  getters: {

  },
  actions: {
    async getCategories({ commit }) {
      try {
        const result = await OpentdbAPI.getCategories();
        if (result.trivia_categories === null || result.trivia_categories.length === 0 ) {
          const message = 'Something went wrong. No categories were returned.';
          commit('setError', message);
          console.log(message);
        } else {
          commit('setCategories', result.trivia_categories);
        }
      } catch (error) {
        commit('setError', error);
        console.log(error);
      }
    },
    async getQuestions({ state, commit }) {
      try {
        const result = await OpentdbAPI.getQuestions(
          state.category, state.difficulty, state.numberOfQuestions);
        if (result.response_code !== 0) {
          const message = `Something went wrong. No questions were returned. Response code ${result.response_code}`;
          commit('setError', message);
          console.log(message);
        } else {
          new AnswerRandomizer().randomizeAnswerOrder(result.results);
          commit('setQuestions', result.results);
        }
      } catch (error) {
        commit('setError', error);
        console.log(error);
      }
    }
  },
});