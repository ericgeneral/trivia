export const OpentdbAPI = {
  getCategories() {
    return fetch('https://opentdb.com/api_category.php')
      .then(response => response.json());
  },
  getQuestions(category, difficulty, numberOfQuestions) {
    const url = `https://opentdb.com/api.php?amount=${numberOfQuestions}&category=${category}&difficulty=${difficulty}`;
    console.log(url);
    return fetch(url)
      .then(response => response.json());
  }
}